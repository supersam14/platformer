import pygame as pg
import random as rn
from os import path
from settings import *
from helpers import *
from sprites import *


class Game:
    def __init__(self):
        # init game window, etc.
        # initialize pg
        pg.init()
        pg.mixer.init()  # start sound system

        # start the canvas, create the window
        if FULLSCREEN:
            self.screen = pg.display.set_mode((WIDTH, HEIGHT), pg.FULLSCREEN)
        else:
            self.screen = pg.display.set_mode((WIDTH, HEIGHT))


        # set title of the window aka caption
        pg.display.set_caption(TITLE)

        # init pg clock
        self.clock = pg.time.Clock()

        self.playing = True # ends level
        self.running = True # ends gameloop

        # new game, resets score etc.
        self.highscore = 0
        self.score = 0

        self.load_data()  # load assets and files


    def new(self):

        self.score = 0

        # Sprite Groups
        # self.all_sprites = pg.sprite.Group()
        # self.platform_sprites = pg.sprite.Group()
        # self.pow_sprites = pg.sprite.Group()
        # self.mob_sprites = pg.sprite.Group()

        # using layers like this:
        # instead of using Groups, using layers. Lower layers are drawn first
        self.all_sprites = pg.sprite.LayeredUpdates()
        self.platform_sprites = pg.sprite.LayeredUpdates()
        self.pow_sprites = pg.sprite.LayeredUpdates()
        self.mob_sprites = pg.sprite.LayeredUpdates()
        self.cloud_sprites = pg.sprite.LayeredUpdates()

        self.player = Player(self)
        self.all_sprites.add(self.player)

        Platform(self, WIDTH / 2, HEIGHT - 10, "start")
        Platform(self, WIDTH * 0.5, HEIGHT - 200, "medium")
        Platform(self, WIDTH * 0.75, HEIGHT * 0.25, "small")
        Platform(self, WIDTH * 0.25, HEIGHT * 0.25, "small")

        # load ingame music
        pg.mixer.music.load(path.join(snd_folder, "music_stepping_up.ogg"))
        pg.mixer.music.play(loops=-1)

        self.gameloop()

    def gameloop(self):
        # gameloop
        # self.show_splash_screen()
        self.playing = True
        while self.playing:
            self.clock.tick(FPS)
            self.events()
            self.update()
            self.draw()

    def update(self):
        # gameloop - update

        # check for collisions
        # player to platforms collision
        if self.player.vel.y > 0: # only check if player moving down
            hits = pg.sprite.spritecollide(self.player, self.platform_sprites, False)
            if hits:
                lowest = hits[0]
                for hit in hits:
                    if hit.rect.top < lowest.rect.top:
                        lowest = hit

                footwidth = 10
                if self.player.pos.x > lowest.rect.left - footwidth and self.player.pos.x < lowest.rect.right + footwidth:
                    self.player.pos.y = lowest.rect.top + 1 # put player ontop of platform
                    self.player.vel.y = 0 # and set speed to 0

        # player to powerups collisions
        hits = pg.sprite.spritecollide(self.player, self.pow_sprites, True)
        for hit in hits:
            if hit.tag == "boost":
                self.jet_sound.play()
                self.player.vel.y = - BOOSTPOWER
                BoostLines(self)

            elif hit.tag == "health":
                self.health_sound.play()
                self.player.health = 100

        # player to mob collision
        hits = pg.sprite.spritecollide(self.player, self.mob_sprites, False)
        if hits:
            m_hits = pg.sprite.spritecollide(self.player, self.mob_sprites, False, pg.sprite.collide_mask)
            if m_hits:
                self.player.hit()

        # scrolling screen
        if self.player.rect.top <= HEIGHT / 4:
            self.player.pos.y += max(abs(self.player.vel.y), 2) # camera should move at same speed as player
            for plat in self.platform_sprites:
                plat.rect.y += max(abs(self.player.vel.y), 2)
            for mob in self.mob_sprites:
                mob.rect.y += max(abs(self.player.vel.y), 2)
            for cloud in self.cloud_sprites:
                cloud.move(max(abs(self.player.vel.y), 2))

            self.score = int(self.score + max(abs(self.player.vel.y) / 10, 2))
            # self.score = int(self.score)

        # delete offscreen Platforms
        for plat in self.platform_sprites:
            if plat.rect.top > HEIGHT + 50:
                plat.kill()
                # self.score += 10


        # spawn new platforms for every killed one
        while len(self.platform_sprites) < PLATFORM_MAXNUM:

            res = []
            res.append("small")
            res.append("medium")
            res.append("large")

            size = rn.choice(res)

            # spawn a platform
            p = Platform(self, rn.randrange(0, WIDTH), rn.randrange(-300, -30), size)

            if rn.randrange(100) < POW_PCT: # spawn a powerup n the platform
                PowBoost(self, p)

            if rn.randrange(100) < POW_PCT:
                PowHealth(self, p)

            if rn.randrange(100) < MOB_PCT and not size == "small": # spawn mob on platform

                Mob(self, p)

        # spawn flymobs
        if rn.randrange(1000) < 10 and len(self.mob_sprites) < 6:
            FlyMob(self)

        # spawn cloud
        if rn.randrange(100) < 10 and len(self.cloud_sprites) < CLOUD_MAXNUM:
            Cloud(self)

        # die
        if self.player.rect.top > HEIGHT + 50:  #if player is below ground
            pg.mixer.music.fadeout(200)
            self.go_sound.play()
            for sprite in self.all_sprites:
                sprite.rect.y -= self.player.vel.y  # move all sprites up
                if sprite.rect.bottom < 0:  # an kill if in the sky
                    sprite.kill()
            if len(self.platform_sprites) <= 0: # if no platforms left, exit level
                self.playing = False

        # if self.player.vel.y < 0:
        #     hits = pg.sprite.spritecollide(self.player, self.platform_sprites, False)
        #     if hits:
        #         self.player.pos.y = hits[0].rect.bottom - self.player.image.get_height()

        # sprite to group collision:

        # hits = pg.sprite.spritecollide(player, mob_sprites, True, pg.sprite.collide_circle)
        # for hit in hits:
        #     ...

        # group to group collision
        # hits = pg.sprite.groupcollide(mob_sprites, bullet_sprites, True, True)
        # for hit in hits:
        #     ...
        self.all_sprites.update()


    def events(self):
        # gameloop - events

        for event in pg.event.get():
            # print(event)
            if event.type == pg.QUIT:
                if self.playing:
                    self.playing = False
                self.running = False
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    if self.playing:
                        self.playing = False
                    self.running = False
                if event.key == pg.K_SPACE:
                    self.player.jump()
                if event.key == pg.K_UP:
                    self.player.jump()


    def draw(self):        ################################ DRAW to canvas #############################################

        self.screen.fill(LIGHTBLUE)
        self.all_sprites.draw(self.screen)

        # take care of background image
        # Screen.fill(BLUE)
        # blit: copy the pixels of one thing to another thing
        # Screen.blit(background, background_rect)

        # gui
        draw_text(self.screen, "Score: " + str(self.score), 30, 65, 5, WHITE)

        # draw_text(self.screen, "Läbe: " + str(self.player.health), 50, WIDTH / 2, 5, WHITE)

        bar_color = GREEN
        if self.player.health < 30:
            bar_color = ORANGE
        if self.player.health < 10:
            bar_color = RED

        draw_bar(self.screen, WIDTH * 0.8, 20, 100, 20, self.player.health, bar_color, BLACK)

        # flip
        pg.display.flip()

    def show_splash_screen(self):
        # _surface.blit(background, background_rect)
        draw_text(self.screen, "Title!", 64, WIDTH / 2, HEIGHT / 4, WHITE)
        draw_text(self.screen, "HIGHSCORE: " + str(self.highscore), 22, WIDTH / 2, HEIGHT / 2, WHITE)
        draw_text(self.screen, "press a key to begin", 22, WIDTH / 2, HEIGHT / 4 * 3, WHITE)
        pg.display.flip()
        self.waiting = True
        while self.waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                if event.type == pg.KEYDOWN:
                    if event.key == pg.K_ESCAPE:
                        pg.quit()
                if event.type == pg.KEYUP:
                    # play appearing sound
                    self.waiting = False

    def show_gameover_screen(self):
        # draw title
        draw_text(self.screen, "Game Over!", 64, WIDTH / 2, HEIGHT /4, RED)

        # check for highscore
        if self.score > self.highscore:
            self.highscore = self.score
            self.save_highscore()
            # write to file here
            draw_text(self.screen, "NEW HIGHSCORE: " + str(self.highscore), 22, WIDTH / 2, HEIGHT / 2, YELLOW)
        else:
            draw_text(self.screen, "HIGHSCORE: " + str(self.highscore), 22, WIDTH / 2, HEIGHT / 2, WHITE)
        draw_text(self.screen, "press a key to continue", 22, WIDTH / 2, HEIGHT / 4 * 3, WHITE)

        # show Screen
        pg.display.flip()

        # waiting loop
        self.waiting = True
        while self.waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                if event.type == pg.KEYDOWN:
                    if event.key == pg.K_ESCAPE:
                        pg.quit()
                if event.type == pg.KEYUP:
                    # play appearing sound"
                    self.waiting = False

    def load_data(self):

        # load the high score
        with open(path.join(game_folder, HS_FILE), "r") as f:  # "w" is for writing, r is for reading
            # if this block of coe ends, the file will be automatically closed
            try:
                self.highscore = int(f.read()) # read returns a string - int() turns it into a number
            except:
                self.highscore = 0

        # load spritesheet (folder and name defined in settings.py, using Spritesheet class
        self.spritesheet_player = Spritesheet(path.join(img_folder, SPRITESHEET_PLAYER))

        # load sound files
        self.jump_sound = pg.mixer.Sound(path.join(snd_folder, "jump_sound.wav"))
        self.go_sound = pg.mixer.Sound(path.join(snd_folder, "go_sound.wav"))
        self.jet_sound = pg.mixer.Sound(path.join(snd_folder, "jet.wav"))
        self.health_sound = pg.mixer.Sound(path.join(snd_folder, "health_sound.wav"))
        self.hit_sound = pg.mixer.Sound(path.join(snd_folder, "hit_sound.wav"))


    def save_highscore(self):
        with open(path.join(game_folder, HS_FILE), "w") as f:
            f.write(str(self.highscore))
            # self.highscore = int(f.read())
