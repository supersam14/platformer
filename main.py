###############################################################
############ TITLE ############################################
###############################################################

# credits
# -

# imports
import pygame as pg
import random as rd
import os
from os import path         # helper lib to help with file locations
from settings import *      # import everything from settings
from game import *

# File I/O

# create insance of game class
game = Game()

game.show_splash_screen()

# starting music
# pg.mixer.music.play(loops = -1) # loop the file

     # game loop #-# game loop #-# game loop #-# game loop #-# game loop #-# game loop #-# game loop #-# game loop #
while game.running:

    game.new()
    game.show_gameover_screen()
# end of game loop
pg.quit()
quit()

