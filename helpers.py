import pygame as pg
from settings import *

# font and text helpers
font_title = pg.font.match_font("arial")  # instead of exatly matching name, this function will take a font
                                          # thats name is similar, ut take a lot of resources --> take out update()


def draw_text(surf, text, size, x, y, color):
    # first thing is fonts. make shure the font is in
    font = pg.font.Font(font_title, size)
    text_surface = font.render(text, True, color) # True to enable anti aliasing
    text_rect = text_surface.get_rect() # get rect
    text_rect.midtop = (x, y) # place rect
    surf.blit(text_surface, text_rect) # draw text

def draw_bar(surf, x, y, width, height, pct, infill_color, outline_color):
    if pct < 0:
        pct = 0
    fill = (pct / 100 * width)
    outline_rect = pg.Rect(x, y, width, height)
    filled_rect = pg.Rect(x, y, fill, height)
    pg.draw.rect(surf, infill_color, filled_rect)
    pg.draw.rect(surf, outline_color, outline_rect, 2) # last number is thickness

class Spritesheet:
    # utility class for loading and parsing spritesheets.
    # parsing means reading through a data file and understanding what it means
    def __init__(self, filename):
        self.spritesheet = pg.image.load(filename).convert()

    def get_image(self, x, y, w, h): # get a slice with this function
        image = pg.Surface((w, h))
        image.blit(self.spritesheet, (0, 0), (x, y, w, h))  # to image copy (blit) from self. spritesheet at
                                                            # location (0, 0) this junk (x, y, w, h))
        image = pg.transform.scale(image, (int(w * SPRITE_SCALE_FACTOR), int(h * SPRITE_SCALE_FACTOR)))
        return image
