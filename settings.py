import os

# global settings

TITLE = "Jumpy!"

# graphics
WIDTH = 640
HEIGHT = 410
FULLSCREEN = False
FPS = 60

# Layer order
PLAYER_LAYER = 2
PLATFORM_LAYER = 1
MOB_LAYER = 2
BG_LAYER = 0

CLOUD_MAXNUM = 15

# color definitions
# colors are set in RGB
# optional parameter is alpha aka transparency
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
FUCHSIA = (225, 0, 255)
LIGHTBLUE = (60, 60, 255)
ORANGE = (255, 165, 0)

# player properties
GRAVITY = 0.5
PLAYER_SCREENWRAP = False
Y_PLAYER_CONSTRAINT = 50
PLAYER_ACC = 0.3
PLAYER_FRICT = -0.08
PLAYER_JUMP_FRC = -15

INVINCIBILITY_TIME = 500

# Starting platforms
PLATFORM_LIST = [(0, HEIGHT - 40, WIDTH, 40),
                 (WIDTH / 2 - 50, HEIGHT * 3 / 4, 100, 20),
                 (125, HEIGHT - 350, 100, 20),
                 (350, 200, 100, 20),
                 (175, 100, 50, 20)]

# platform properties
PLATFORM_WMIN = 30
PLATFORM_WMAX = 50
PLATFORM_H = 20
PLATFORM_MAXNUM = 8

# Powerup settings
POW_PCT = 10
BOOSTPOWER = 30

# Mob settings
MOB_PCT = 15

FLYMOB_TIME = 6000
FLYMOB_MAX_AMT = 3
FLYMOB_SPEED = 2

# sprite import options

SPRITE_SCALE_FACTOR = 0.9

# setup asset folders

# this is the Folder where this script is located
game_folder = os.path.dirname(__file__)
# define the graphics folder
img_folder = os.path.join(game_folder, "img")
# define sound folder
snd_folder = os.path.join(game_folder, "snd")

HS_FILE = "highscore.txt"

SPRITESHEET_PLAYER = "master_sheet.png"