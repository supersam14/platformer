# sprite classes for game
import pygame as pg
from settings import *
from os import path
import random as rn
vec = pg.math.Vector2


class Player(pg.sprite.Sprite):
    def __init__(self, game):
        self._layer = PLAYER_LAYER
        self.groups = game.all_sprites # add sprite groups here this sprite should be automatically joined to
        pg.sprite.Sprite.__init__(self, self.groups) # and pass in here
        self.game = game # referencing game to use platform_sprites

        self.xamt = 15
        self.height = 99
        self.xbord = 1

        # self.image = pg.Surface((30,40))
        self.load_images()
        self.image = self.standing_frames[1]
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()

        self.pos = vec(50 , HEIGHT -50) # using vectors
        self.vel = vec(0.0, 0.0)
        self.acc = vec(0.0, 0.0)

        # animation framework
        self.current_frame = 0
        self.last_update = 0

        self.health = 100
        self.invincible = False
        self.last_hit = pg.time.get_ticks()

        self.walking = False
        self.jumping = False
        self.falling = False

    def load_images(self):

        self.standing_frames = [self.game.spritesheet_player.get_image(0, 0, 67, 94),
                                self.game.spritesheet_player.get_image(80, 0, 67, 94)]

        for frame in self.standing_frames:
            frame.set_colorkey(BLACK)

        self.walk_frames_r = [self.game.spritesheet_player.get_image(412, 0, 70, 98),
                               self.game.spritesheet_player.get_image(495, 0, 75, 98)]

        for frame in self.walk_frames_r:
            frame.set_colorkey(BLACK)

        self.walk_frames_l = []

        for frame in self.walk_frames_r:
            self.walk_frames_l.append(pg.transform.flip(frame, True, False))

        for frame in self.walk_frames_l:
            frame.set_colorkey(BLACK)

        self.jumping_frames_r = [self.game.spritesheet_player.get_image(158, 0, 71, 98),
                               self.game.spritesheet_player.get_image(321, 0, 71, 98)]

        for frame in self.jumping_frames_r:
            frame.set_colorkey(BLACK)

        self.jumping_frames_l = []

        for frame in self.jumping_frames_r:
            self.jumping_frames_l.append(pg.transform.flip(frame, True, False))

        for frame in self.jumping_frames_l:
            frame.set_colorkey(BLACK)

    def update(self):

        self.animate()

        # default acc to 0
        self.acc = vec(0, GRAVITY)

        # get inputs
        keys = pg.key.get_pressed()
        if keys[pg.K_LEFT]:
            self.acc.x = -PLAYER_ACC
        if keys[pg.K_RIGHT]:
            self.acc.x = PLAYER_ACC

        # apply friction
        self.acc.x += self.vel.x * PLAYER_FRICT

        # equations of motion
        self.vel += self.acc
        self.pos += self.vel + 0.5 * self.acc

        # wrap around the sides of the screen
        if self.pos.x > WIDTH:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = WIDTH

        # set position
        self.rect.midbottom = self.pos

        # check invincibility

        if self.invincible and self.last_hit + INVINCIBILITY_TIME < pg.time.get_ticks():
            self.invincible = False

    def animate(self):

        # set animation vars
        if abs(self.vel.x) > 0.5 and self.vel.y == 0:
            self.walking = True
        else:
            self.walking = False

        self.jumping = True
        if self.vel.y == 0:
            self.jumping = False

        if self.jumping:
            if self.vel.y > 0:
                self.falling = True
            else:
                self.falling = False

        # animate
        now = pg.time.get_ticks()
        # standing frames
        if not self.walking and not self.jumping: # aka if standing
            if now - self.last_update > 400:  # if its time to change: (time in millis)
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.standing_frames) # go to the next frame
                self.image = self.standing_frames[self.current_frame]

        # walking left
        if self.walking and self.vel.x < 0:
            if now - self.last_update > 150:  # if its time to change: (time in millis)
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.walk_frames_l) # go to the next frame
                self.image = self.walk_frames_l[self.current_frame]

        # walking right
        if self.walking and self.vel.x > 0:
            if now - self.last_update > 150:  # if its time to change: (time in millis)
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.walk_frames_r) # go to the next frame
                self.image = self.walk_frames_r[self.current_frame]

        # jumping left
        if not self.walking and self.vel.x < 0:
            if self.vel.y < 0:
                self.image = self.jumping_frames_l[0]
            elif self.vel.y > 0:
                self.image = self.jumping_frames_l[1]

        # jumping right
        if not self.walking and self.vel.x > 0:
            if self.vel.y < 0:
                self.image = self.jumping_frames_r[0]
            elif self.vel.y > 0:
                self.image = self.jumping_frames_r[1]

        # setting collision mask
        self.mask = pg.mask.from_surface(self.image)

    def jump(self):
        # jump only if standing on a platform
        self.rect.y += 3
        hits = pg.sprite.spritecollide(self, self.game.platform_sprites, False)
        self.rect.y -= 3
        if hits:
            self.game.jump_sound.play()
            self.vel.y = PLAYER_JUMP_FRC

    def hit(self):
        if not self.invincible:
            self.game.hit_sound.play()
            self.health -= 20
            self.invincible = True
            self.last_hit = pg.time.get_ticks()
            if self.health < 0:
                self.game.playing = False;


class Platform(pg.sprite.Sprite):
    def __init__(self, game, x, y, s):
        self._layer = PLATFORM_LAYER
        self.groups = game.all_sprites, game.platform_sprites # add sprite groups here this sprite should be automatically joined to
        pg.sprite.Sprite.__init__(self, self.groups) # and pass in here
        self.game = game  # referencing game to use platform_sprites
        if s == "small":
            self.image = pg.image.load(path.join(img_folder, "plat_small.png")).convert()
        if s == "medium":
            self.image = pg.image.load(path.join(img_folder, "plat_medium.png")).convert()
        if s == "large":
            self.image = pg.image.load(path.join(img_folder, "plat_large.png")).convert()
        if s == "start":
            self.image = pg.image.load(path.join(img_folder, "plat_start.png")).convert()

        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.centerx = x
        self.rect.centery = y


class PowHealth(pg.sprite.Sprite):
    def __init__(self, game, plat): # pass it platform, because it will allways spawn there
        self._layer = PLATFORM_LAYER # draw layer is set here
        self.groups = game.all_sprites, game.pow_sprites # add sprite groups here this sprite should be automatically joined to
        pg.sprite.Sprite.__init__(self, self.groups) # and pass in here
        self.game = game # passing vars
        self.plat = plat

        self.tag = "health"

        self.current_frame = 0  # animation framework
        self.last_update = pg.time.get_ticks()

        self.frames = []
        self.frames.append(pg.image.load(path.join(img_folder, "gemRed.png")).convert())
        self.frames.append(pg.image.load(path.join(img_folder, "gemBlue.png")).convert())
        self.frames[0].set_colorkey(BLACK)
        self.frames[1].set_colorkey(BLACK)

        self.image = self.frames[self.current_frame]

        self.rect = self.image.get_rect()
        self.rect.bottom = self.plat.rect.top + 30
        self.rect.centerx = rn.randrange(self.plat.rect.left, self.plat.rect.right)

    def update(self):
        self.animate()
        self.rect.bottom = self.plat.rect.top
        if not self.game.platform_sprites.has(self.plat):
            self.kill()

    def animate(self):
        now = pg.time.get_ticks()
        # standing frames
        if now - self.last_update > 150:  # if its time to change: (time in millis)
            self.last_update = now
            self.current_frame = (self.current_frame + 1) % len(self.frames) # go to the next frame
            self.image = self.frames[self.current_frame]


class PowBoost(pg.sprite.Sprite):
    def __init__(self, game, plat): # pass it platform, because it will allways spawn there
        self._layer = PLATFORM_LAYER
        self.groups = game.all_sprites, game.pow_sprites # add sprite groups here this sprite should be automatically joined to
        pg.sprite.Sprite.__init__(self, self.groups) # and pass in here
        self.game = game # passing vars
        self.plat = plat

        self.tag = "boost"

        self.current_frame = 0  # animation framework
        self.last_update = pg.time.get_ticks()

        self.frames = []
        self.frames.append(pg.image.load(path.join(img_folder, "star.png")).convert())
        self.frames.append(pg.image.load(path.join(img_folder, "star2.png")).convert())
        self.frames[0].set_colorkey(WHITE)
        self.frames[1].set_colorkey(BLACK)

        self.image = self.frames[self.current_frame]

        self.rect = self.image.get_rect()
        self.rect.bottom = self.plat.rect.top + 30
        self.rect.centerx = rn.randrange(self.plat.rect.left, self.plat.rect.right)

    def update(self):
        self.animate()
        self.rect.bottom = self.plat.rect.top
        if not self.game.platform_sprites.has(self.plat):
            self.kill()

    def animate(self):
        now = pg.time.get_ticks()
        # standing frames
        if now - self.last_update > 150:  # if its time to change: (time in millis)
            self.last_update = now
            self.current_frame = (self.current_frame + 1) % len(self.frames) # go to the next frame
            self.image = self.frames[self.current_frame]


class BoostLines(pg.sprite.Sprite):
    def __init__(self, game):
        self._layer = PLAYER_LAYER
        self.groups = game.all_sprites  # add sprite groups here this sprite should be automatically joined to
        pg.sprite.Sprite.__init__(self, self.groups)  # and pass in here
        self.game = game  # passing vars

        self.start_time = pg.time.get_ticks()

        self.last_update = pg.time.get_ticks()
        self.current_frame = 0
        self.frames = []

        self.frames.append(pg.image.load(path.join(img_folder, "boost_lines0.png")).convert())
        self.frames.append(pg.image.load(path.join(img_folder, "boost_lines1.png")).convert())
        self.frames.append(pg.image.load(path.join(img_folder, "boost_lines2.png")).convert())

        self.frames[0].set_colorkey(WHITE)
        self.frames[1].set_colorkey(WHITE)
        self.frames[2].set_colorkey(BLACK)

        self.image = self.frames[self.current_frame]

        self.rect = self.image.get_rect()

        self.rect.top = self.game.player.rect.bottom
        self.rect.midtop = self.game.player.rect.midbottom

    def update(self):
        self.game.player.invincible = True
        self.rect.midtop = self.game.player.rect.midbottom
        self.animate()

        if self.start_time + 500 < pg.time.get_ticks():
            self.kill()

    def animate(self):
        now = pg.time.get_ticks()
        # standing frames
        if now - self.last_update > 100:  # if its time to change: (time in millis)
            self.last_update = now
            self.current_frame = (self.current_frame + 1) % len(self.frames)  # go to the next frame
            self.image = self.frames[self.current_frame]


class Mob(pg.sprite.Sprite):
    def __init__(self, game, plat):
        self._layer = MOB_LAYER
        self.groups = game.all_sprites, game.mob_sprites  # add sprite groups here this sprite should be automatically joined to
        pg.sprite.Sprite.__init__(self, self.groups)  # and pass in here
        self.game = game  # passing vars
        self.plat = plat

        self.last_update = pg.time.get_ticks()
        self.current_frame = 0
        self.left_frames = []

        self.left_frames.append(pg.image.load(path.join(img_folder, "slimeWalk1.png")).convert())
        self.left_frames.append(pg.image.load(path.join(img_folder, "slimeWalk2.png")).convert())

        self.left_frames[0].set_colorkey(BLACK)
        self.left_frames[1].set_colorkey(BLACK)

        self.right_frames = []

        for frame in self.left_frames:
            self.right_frames.append(pg.transform.flip(frame, True, False))
            frame.set_colorkey(BLACK)

        self.image = self.left_frames[self.current_frame]

        self.rect = self.image.get_rect()

        self.rect.bottom = self.plat.rect.top + 2
        self.rect.centerx = rn.randrange(max(self.plat.rect.left + 20, 0), max(self.plat.rect.right - 20, 0))

        self.moving_r = rn.choice((True, False))

    def update(self):

        self.animate()
        # check for rim
        if self.moving_r:
            self.rect.centerx += 50
            self.rect.centery += 10
            hits = pg.sprite.spritecollide(self, self.game.platform_sprites, False)
            self.rect.centerx -= 50
            self.rect.centery -= 10

            if hits:
                self.rect.centerx += 1
            else:
                self.moving_r = False
                self.image = self.left_frames[0]

        else:
            self.rect.centerx -= 50
            self.rect.centery += 10
            hits = pg.sprite.spritecollide(self, self.game.platform_sprites, False)
            self.rect.centerx += 50
            self.rect.centery -= 10

            if hits:
                self.rect.centerx -= 1
            else:
                self.moving_r = True
                self.image = self.right_frames[0]

        # self destruction
        if self.rect.top > HEIGHT:
            self.kill()

    def animate(self):
        now = pg.time.get_ticks()
        if self.moving_r:
            if now - self.last_update > 300:  # if its time to change: (time in millis)
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.right_frames)  # go to the next frame
                self.image = self.right_frames[self.current_frame]
        else:
            if now - self.last_update > 300:  # if its time to change: (time in millis)
                self.last_update = now
                self.current_frame = (self.current_frame + 1) % len(self.left_frames)  # go to the next frame
                self.image = self.left_frames[self.current_frame]

        self.mask = pg.mask.from_surface(self.image)


class FlyMob(pg.sprite.Sprite):
    def __init__(self, game):
        self._layer = MOB_LAYER
        self.groups = game.all_sprites, game.mob_sprites  # add sprite groups here this sprite should be automatically joined to
        pg.sprite.Sprite.__init__(self, self.groups)  # and pass in here
        self.game = game  # passing vars

        self.current_frame = 0
        self.last_update = 0
        self.frames = []
        self.frames.append(pg.image.load(path.join(img_folder, "fly1.png")).convert())
        self.frames.append(pg.image.load(path.join(img_folder, "fly2.png")).convert())

        self.frames[0].set_colorkey(BLACK)
        self.frames[1].set_colorkey(BLACK)

        self.image = self.frames[self.current_frame]

        self.rect = self.image.get_rect()

        self.rect.bottom = rn.randrange(-300, 0)

        self.flying_r = False
        if rn.randrange(100) > 50:
            self.flying_r = True
            l_frames = []
            for frame in self.frames:
                l_frames.append(pg.transform.flip(frame, True, False))
            self.frames.clear()
            self.frames = l_frames
            self.rect.centerx = -100
        else:
            self.flying_r = False
            self.rect.centerx = WIDTH + 100

        print("flymob spawned")

    def update(self):
        if self.flying_r:
            self.rect.centerx = self.rect.centerx + FLYMOB_SPEED

        else:
            self.rect.centerx = self.rect.centerx - FLYMOB_SPEED

        self.animate()

        # self destruction
        if self.rect.centerx < -100 or self.rect.centerx > WIDTH + 100 or self.rect.top > HEIGHT + 10:
            self.kill()
    def animate(self):
        now = pg.time.get_ticks()
        # standing frames
        if now - self.last_update > 300:  # if its time to change: (time in millis)
            self.last_update = now
            self.current_frame = (self.current_frame + 1) % len(self.frames)  # go to the next frame
            self.image = self.frames[self.current_frame]

        self.mask = pg.mask.from_surface(self.image)


class Cloud(pg.sprite.Sprite):
    def __init__(self, game):
        self._layer = BG_LAYER
        self.groups = game.all_sprites, game.cloud_sprites  # add sprite groups here this sprite should be automatically joined to
        pg.sprite.Sprite.__init__(self, self.groups)  # and pass in here
        self.game = game  # passing vars

        self.dist = rn.randrange(20, 80) / 100

        self.images = []
        self.images.append(pg.image.load(path.join(img_folder, "cloud1.png")).convert())
        self.images.append(pg.image.load(path.join(img_folder, "cloud2.png")).convert())
        self.images.append(pg.image.load(path.join(img_folder, "cloud3.png")).convert())

        for image in self.images:
            image.set_colorkey(BLACK)

        self.image = rn.choice(self.images)
        self.rect = self.image.get_rect()
        self.image = pg.transform.scale(self.image, (int(self.rect.width * self.dist * 2), int(self.rect.height * self.dist * 2)))
        self.rect = self.image.get_rect()
        self.rect.bottom = rn.randrange(-1000, 0)
        self.rect.centerx = rn.randrange(0, WIDTH)

    def update(self):
        if self.rect.top > HEIGHT * 2:
            self.kill()

    def move(self, speed):
        dist = speed
        self.rect.centery += abs(speed * self.dist)